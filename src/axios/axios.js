// 安装axios(基于promise的HTTP库)
import axios from 'axios';
import Vue from 'vue';

// 开发环境的地址，在生产环境打包之前注释这一行
// axios.defaults.baseURL = 'http://104.168.28.214/';
// axios.defaults.baseURL = 'http://localhost:8099';

export default axios;