import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

let roots = (resolve, reject) => {
  return require.ensure([], () => {
    resolve(require('@/views/roots/roots.vue'));
  })
}
let home = (resolve, reject) => {
  return require.ensure([], () => {
    resolve(require('@/views/home/home.vue'));
  })
}
let register = (resolve, reject) => {
  return require.ensure([], () => {
    resolve(require('@/views/register/register.vue'));
  })
}
let login = (resolve, reject) => {
  return require.ensure([], () => {
    resolve(require('@/views/login/login.vue'));
  })
}
let error = (resolve, reject) => {
  return require.ensure([], () => {
    resolve(require('@/views/error/error.vue'));
  })
}

Vue.use(Router)

let router = new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'roots',
      component: roots,
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '*',
      name: 'error',
      component: error
    }
  ]
})

// 全局导航跳转之前和之后的钩子函数
router.beforeEach((to, from, next) => {
  // store.dispatch('getUserInfoList');
  // setTimeout(() => {
  //   // 如果不是登录页面
  //   if(to.path != '/login'){  
  //     if(store.state.login.user_status != 0){
  //       router.app.$message({
  //         type: 'error',
  //         message: '请登录',
  //         duration: 1300
  //       })
  //       setTimeout(() => {
  //         router.app.$router.push({name: 'login'});
  //       })
  //       next(); 
  //     }else {
  //       next();
  //     }
  //   }else {
  //     if(store.state.login.user_status != 0){
  //       next();
  //     }else {
  //       router.app.$message({
  //         type: 'success',
  //         message: '您已登录',
  //         duration: 1300
  //       })
  //       setTimeout(() => {
  //         router.app.$router.push({name: 'home'});
  //       }, 1600)
  //     }
  //   }
  // })
})

export default router