export default {
    name: 'register',
    data(){
        return {
            labelPosition: 'right',
            formLabelAlign: {
                username: '',
                password: '',
                passwordagain: '',
                email: '',
            },
            rules: {
                username: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入用户名'));
                        }else{
                            if (/^[a-zA-Z0-9_-]{4,16}$/.test(value)) {
                                callback();
                            } else {
                                callback(new Error('用户名由4-16位(字母，数字，下划线，减号)组成'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
                password: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入密码'));
                        }else{
                            // 密码必须是8-16位，包括大小写字母和数字
                            // /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{8,16}$/
                            // 8到16位数字与字母组合
                            if (/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/.test(value)) {
                                callback();
                            } else {
                                callback(new Error('密码是6到16位数字与字母组合'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
                passwordagain: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入重复密码'));
                        }else{
                            if (value == this.formLabelAlign.password) {
                                callback();
                            } else {
                                callback(new Error('两次输入密码不一致'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
                email: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入邮箱'));
                        }else{
                            if (/\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/.test(value)) {
                                callback();
                            } else {
                                callback(new Error('请输入正确的邮箱地址'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
            }
        }
    },
    methods: {
        submit(formData){
            this.$refs[formData].validate((valid) => {
                if(valid){
                    this.$axios.post('/APIregister',this.formLabelAlign)
                        .then((data) => {
                            if(data.data.Status == 0){
                                this.$message({
                                    message: '注册成功，前往登录页面',
                                    type: 'success'
                                })
                                setTimeout(() => {
                                    this.$router.push({name: 'login'})
                                },1000)
                            }else {
                                this.$message.error(data.data.Msg);
                            }
                        })
                        .catch((error) => {
                            this.$message({
                                message: '服务器错误',
                                type: 'error'
                            });
                        })
                }else {
                    this.$message({
                        message: '请输入正确的内容',
                        type: 'error'
                    })
                }
            })
        },
        goLogin(){
            this.$router.push({name: 'login'});
        }
    },
    computed: {
        
    }
}