export default {
    name: 'login',
    data(){
        return {
            labelPosition: 'right',
            formLabelAlign: {
                username: '',
                password: '',
            },
            rules: {
                username: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入用户名'));
                        }else{
                            if (/^[a-zA-Z0-9_-]{4,16}$/.test(value)) {
                                callback();
                            } else {
                                callback(new Error('用户名由4-16位(字母，数字，下划线，减号)组成'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
                password: [{
                    validator: (rule, value, callback) => {
                        if (value === '') {
                            callback(new Error('请输入密码'));
                        }else{
                            if (/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/.test(value)) {
                                callback();
                            } else {
                                callback(new Error('密码是6到16位数字与字母组合'));
                            }
                        }
                    },trigger: 'blur',required: true
                }],
            }
        }
    },
    methods: {
        loginIn(formData){
            this.$refs[formData].validate((valid) => {
                if(valid){
                    this.$axios.post('/APIlogin',this.formLabelAlign)
                        .then((data) => {
                            if(data.data.Status == 0){
                                this.$message({
                                    message: '登录成功',
                                    type: 'success'
                                })
                                this.$store.dispatch('getUserInfoList');
                                setTimeout(() => {
                                    this.$router.push({name: 'home'})
                                }, 1000)
                            }else {
                                this.$message.error(data.data.Msg);
                            }
                        })
                        .catch((error) => {
                            this.$message({
                                message: '服务器错误',
                                type: 'error'
                            });
                        })
                }else {
                    this.$message({
                        message: '请输入正确的内容',
                        type: 'error'
                    })
                }
            })
        },
        goRegister(){
            this.$router.push({name: 'register'});
        }
    },
    computed: {
        
    }
}