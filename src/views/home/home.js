export default {
    name: 'home',
    methods: {
        goLogin(){
            this.$router.push({name: 'login'})
        },
        goRoots(){
            this.$router.push({name: 'roots'})
        }
    },
    computed: {
        user_status(){
            return this.$store.state.login.user_status
        },
        user_login(){
            if(this.$store.state.login.user_status == 0){
                return this.$store.state.login.user_login
            }
        },
        user_nickname(){
            if(this.$store.state.login.user_status == 0){
                return this.$store.state.login.user_nickname
            }
        },
        user_email(){
            if(this.$store.state.login.user_status == 0){
                return this.$store.state.login.user_email
            }
        },

    }
}