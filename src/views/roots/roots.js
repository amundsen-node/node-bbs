export default {
    name: 'roots',
    data(){
        return {
            activeNames: ['0'],
        }
    },
    beforeCreate () {
        this.$store.dispatch('getUserInfoList');
        this.$store.dispatch('getContentList');
    },
    methods: {
        goLogin(){
            this.$router.push({name: 'login'})
        },
        goRegister(){
            this.$router.push({name: 'register'})
        },
        goHome(){
            if(this.$store.state.login.user_status == 0){
                this.$router.push({name: 'home'})
            }else {
                this.$message({
                    message: '您未登录，将跳转到登录页面',
                    type: 'warning',
                    duration: 1400
                });
                setTimeout(() => {
                    this.$router.push({name: 'login'})
                },1600)
            }
        },
        handleChange(val){
            console.log(val)
        }
    },
    computed: {
        author(){
            return this.$store.state.roots.author
        },
        wc(){
            return this.$store.state.roots.wc
        },
        curr(){
            return this.$store.state.roots.curr
        },
        title(){
            return this.$store.state.roots.title
        },
        content(){
            return this.$store.state.roots.content
        } 
    }
}