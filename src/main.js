// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// 全局样式
import '@/assets/css/app.less';

// 引入element-ui框架
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

// 引入全局axios
import axios from './axios'
Vue.use({
  install: function(vm, options){
    vm.prototype.$axios = axios;
  }
});

// 引入store(全局状态管理)
import store from './store'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // 注册状态管理
  store,
  // 注册路由到Vue实例中
  router,
  template: '<App/>',
  components: { App }
})