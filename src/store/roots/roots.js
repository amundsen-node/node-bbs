import axios from 'axios'
export default {
    state: {
        author: '',
        wc: 0,
        curr: '',
        title: '',
        content: '',
        digest: '',
    },
    mutations: {
        getContent(state, data){
            state.author = data.author;
            state.wc = data.wc;
            state.curr = data.date.curr;
            state.title = data.title;
            state.content = data.content;
            state.digest = data.digest;
        }
    },
    actions: {
        getContentList({commit}){
            axios.get('/APIOneArticle')
                .then((data) => {
                    commit('getContent', data.data.data);
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    }
}