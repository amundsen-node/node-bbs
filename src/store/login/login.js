import axios from 'axios'
export default {
    state: {
        user_status: null,
        user_login: '',
        user_nickname: '',
        user_email: '',
    },
    mutations: {
        getUserInfo(state, data){
            state.user_status = 0;
            state.user_login = data.user_login;
            state.user_nickname = data.user_nickname;
            state.user_email = data.user_email;
        },
        getUserInfoNull(state){
            state.user_status = 1;
        }
    },
    actions: {
        // {commit}: 对象的解构赋值
        getUserInfoList({commit}){
            console.log('请求用户状态、信息')
            axios.get('/APIislogin')
                .then((data) => {
                    if(data.data.Status == 0){
                        commit('getUserInfo', data.data.Data);
                    }else if(data.data.Status != 0) {
                        commit('getUserInfoNull')
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    }
}