import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import roots from './roots'
import login from './login'

// 定义一个容器
let store = new Vuex.Store({
    // 应用中的状态数据都存放在state中，相当于data
    state: {

    },
    // 我们可以使用getters对状态state进行进一步的处理，相当于计算属性
    getters: {
        
    },
    // mutations：唯一修改状态的事件回调函数（同步）
    mutations: {
        
    },
    // 使用actions来处理异步操作，并且需要提交一个mutations。请结合控制台的vuex插件看state的改变。
    actions: {
        
    },
    modules: {
        roots,
        login,
        // 把不同的业务逻辑拆分，使用模块化方法管理/保存/处理Vuex，使得代码逻辑清晰明确，结构代码整齐规划
        // 例子：组件访问拆分的模块的state时候，应该访问this.$store.yourModule.state...
        // 请查看文档(路由也可以拆分模块管理)。
    }
})
// 流程：Vue组件请求后端接口，通过actions异步操作获取数据，提交mutation(Devtools会监控mutation的变化)，从而改变state状态，最后组件中渲染出对应的数据。

export default store